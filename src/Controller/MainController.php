<?php

namespace RenderScript\Api\Engine\Controller;

use OAuth2\Request;

class MainController
{
    private $OAuthServer;

    protected $success = true;
    protected $code = 200;
    protected $message = 'Success';
    protected $data = [];

    public function __construct()
    {
        $this->OAuthServer = require_once dirname(dirname(__DIR__)) . '/server.php';
    }

    protected function authorize()
    {
        if (!$this->OAuthServer->verifyResourceRequest(Request::createFromGlobals())) {
            /** @var \OAuth2\Response $response */
            $response = $this->OAuthServer->getResponse();

            $this->success = false;
            $this->code = $response->getStatusCode();
            $this->message = $response->getStatusText();

            $this->sendResponse();
            exit;
        }

    }

    protected function sendResponse()
    {
        echo json_encode([
            'success' => $this->success,
            'code' => $this->code,
            'message' => $this->message,
            'data' => $this->data,
        ]);
    }

    public function default($callable = null)
    {
        $this->authorize();

        if($callable !== null){
            $callable();
        }

        $this->sendResponse();
    }

    public function token()
    {
        $this->OAuthServer->handleTokenRequest(Request::createFromGlobals())->send();
    }
}